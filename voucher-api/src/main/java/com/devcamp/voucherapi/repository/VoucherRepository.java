package com.devcamp.voucherapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.voucherapi.model.Voucher;

public interface VoucherRepository extends JpaRepository<Voucher, Long> {

}
