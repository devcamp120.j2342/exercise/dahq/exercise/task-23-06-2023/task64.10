package com.devcamp.voucherapi.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.voucherapi.model.Voucher;
import com.devcamp.voucherapi.repository.VoucherRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class VoucherController {
    @Autowired
    VoucherRepository voucherRepository;

    // lay tat ca danh sach voucher
    @GetMapping("/vouchers")
    public ResponseEntity<List<Voucher>> getAllVoucher() {
        try {
            List<Voucher> vouchers = new ArrayList<Voucher>();
            voucherRepository.findAll().forEach(vouchers::add);
            return new ResponseEntity<>(vouchers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    // lay voucher theo id
    @GetMapping("/vouchers/{id}")
    public ResponseEntity<Voucher> getVoucherById(@PathVariable("id") long id) {
        try {
            Optional<Voucher> vouchers = voucherRepository.findById(id);
            if (vouchers.isPresent()) {
                return new ResponseEntity<>(vouchers.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // tao voucher Post
    @PostMapping("/vouchers")
    public ResponseEntity<Voucher> CreateVoucher(@Valid @RequestBody Voucher cVoucher) {
        try {

            cVoucher.setNgayTao(new Date());
            cVoucher.setNgayCapNhat(null);
            Voucher pVoucher = voucherRepository.save(cVoucher);
            return new ResponseEntity<>(pVoucher, HttpStatus.CREATED);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // sua voucher Put
    @PutMapping("/vouchers/{id}")
    public ResponseEntity<Object> UpdateVoucher(@RequestBody Voucher pVoucher, @PathVariable("id") long id) {
        Optional<Voucher> cVoucher = voucherRepository.findById(id);
        if (cVoucher.isPresent()) {
            Voucher bVoucher = cVoucher.get();
            bVoucher.setMaVoucher(pVoucher.getMaVoucher());
            bVoucher.setGhiChu(pVoucher.getGhiChu());
            bVoucher.setPhanTramGiamGia(pVoucher.getPhanTramGiamGia());
            try {

                return new ResponseEntity<>(voucherRepository.save(bVoucher), HttpStatus.OK);

            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to update voucher: " + e.getCause().getCause().getMessage());
            }
        } else {
            return ResponseEntity.badRequest().body("Failed to get Update voucher: " + id + "for update.");
        }

    }

    // delete voucher
    @DeleteMapping("/vouchers/{id}")
    public ResponseEntity<String> deleteVoucher(@PathVariable("id") long id) {
        try {
            voucherRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
